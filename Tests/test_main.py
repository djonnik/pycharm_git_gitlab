import pytest

from Main import summ


def test_main():
    assert summ(1, 1) == 2


def test_main_0():
    assert summ(0, 0) == 0


def test_main_reus():
    assert summ(1, -1) == 0


if __name__ == "__main__":
    pytest.main()
